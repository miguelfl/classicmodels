<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
   <sql:query var="modelos" dataSource="jdbc/classicmodels">
   	 select productName, productScale, productDescription, productCode, MSRP from products;
   </sql:query> 
   
   <!DOCTYPE html> 
   <html>
    <head>
    	 <meta charset="ISO-8859-1">
    	 <title>Catálogo de Productos</title>
    	 <style>
    	 	*{
    	 		background-color:#FAFDA9 ;
    	 		font-size:1em;
    	 		font-family:Arial;
    	 		margin-right:5%;
    	 		margin-left:5%;
    	 	}
    	 	
    	 	p{
  line-height: 200%;
    	 	font-weight:normal;
    	 	
    	 	}
    	 	.cursiva{
    	 		font-style:italic;
    	 	}
    	 	h1{
    	 	color:#A4A938;
    	 		font-size:3em;
    	 		text-align:center;
    	 		font-family:serif;
    	 	}
    	 	h3{
    	 		color:#A4A938;
    	 		 font-size:2em;
    	 		font-family:serif;
    	 		  	 	}
    	 	hr{
    	 	border-color:black;
    	 	}
    	 </style>
   	</head>
   	 <body> <h1>Catálogo de Modelos a Escala</h1>
   	 <hr/>
   	 <c:choose>
   	 	<c:when test="${not empty sessionScope.customer }">
   	 	<p>${customer}</p>
   	 	<p><a href="cart.jsp">Carrito</a>
   	 	<p><a href="logout.jsp">Cerrar sesion</a>
   	 	</c:when>
   	 	<c:otherwise>
   	 		<p><a href="login.jsp">Iniciar Sesion</a></p>
   	 		<p><a href="registro.jsp">Registrarse</a></p>
   	 		
   	 	</c:otherwise>
   	 </c:choose>
   	 
   	 
   	 
   	  <c:forEach var="modelo" items="${modelos.rows}">
   	  <h3><c:out value="${modelo.productName}" /></h3>
   	  <p class="cursiva">Escala <c:out value="${modelo.productScale}" /></p>
   	  <p><c:out value="${modelo.productDescription}" /></p>
   	  <p>
   	  	Precio: ${modelo.buyPrice}
   	  	<c:if test="${not empty sessionScope.customer }">
   	  		<a href="cart?c=${modelo.productCode}&n=${modelo.productName}&p=${modelo.MSRP}">
   	  			<img style="width:1em; height:1em" src="img/shoppingcart.svg"/>
   	  		</a>
   	  	</c:if>
   	  </p>
   	  <hr/>
   	  </c:forEach>
   	  </body>
   	  </html>