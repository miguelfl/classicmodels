
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!-- Poner esta redireccion para que si alguien intenta meterse al carrito
   sin la sesion iniciada le redireccione al catalogo -->
<c:if test="${not empty sessionScoupe.customer}">
	<c:redirect url="catalogo.jsp" />
</c:if>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Carrito</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   <script>
   var x="."+${linea.value.productName};
   $(".boton").click(function(){
	   $('x').remove();
   })
   </script>
</head>
<body>
	<h1>Carrito de la compra</h1>
	<p><a href="catalogo.jsp">Catalogo</a></p>
	<!-- Si no existe esta variable de sesion -->
	<c:choose>
		<c:when test="${empty sessionScope.cart}">
			<p>No se han añadido productos al carrito</p>
		</c:when>
		<c:otherwise>
			<table>
				<tr>
					<th>Producto</th>
					<th>Unidades</th>
					<th>Precio</th>
					<th>Importe</th>
					<c:forEach var="linea" items="${sessionScope.cart}">
						<tr class='${linea.value.productName}'>
							<td>${linea.value.productName}</td>
							<td>${linea.value.amount}</td>
							<td>${linea.value.price}</td>
							<td>${linea.value.amount * linea.value.price}</td>
							<td><button class='boton' onclick="">Eliminar</button></td>
						</tr>
					</c:forEach>
				</tr>
			</table>
		</c:otherwise>
	</c:choose>



</body>
</html>